# For...Of

**It project about cycle for... of  in JS**

# Сводка

>Оператор for...of выполняет цикл обхода итерируемых объектов (включая Array, Map, Set, объект аргументов и подобных), вызывая на каждом шаге итерации операторы для каждого значения из различных свойств объекта.

```html
for (variable of iterable) {
  statement
}
```
**variable**
>На каждом шаге итерации variable присваивается значение нового свойства объекта iterable. Переменная variable может быть также объявлена с помощью const, let или var.

**iterable**
>Объект, перечисляемые свойства которого обходятся во время выполнения цикла.

# Под капотом

> В ES6 у **объектов** появился метод **[Symbol.iterator]()** называется итерируемым. 
Не только в for-of, но и в конструкторах Map и Set, деструктурирующем присваивании и в новом операторе распространения (spread operator) 


## Пример
> Рассмотрим как работает данный for..of в эквиваленте **while(){}**
> Для реализации этого метода мы присвоим переменной **iterator** итерируемый объект с методом **[Symbol.iterator]** Он возвращает объект-итератор.

```html
let fullObject = [{one:"b"}, {two: "b"}, {three: "c"}]
let iterator = fullObject[Symbol.iterator]()
let result = iterator.next();
while (!result.done) {
    variable = result.value;
    console.log(variable)
    result = iterator.next();
}
```
 
> Итератором может быть любой объект с методом .next()
> Метод .next() выглядет таким образом.

```html

Object{
  next: function(){
    return {done: false, value: iterator.variable};
  }
}
```

>Наш метод **next()** возвращает объект где :

```html
{ 
  done: false,
  value: iterator.variable
}
```
> данный цикл будет продолжаться до тех пор пока метод **next()** будет возвращать **{ done : false**, следующее значение —  ** value = iterator.value}**
> Всякий раз когда $итератор.next() тоесть done: (будет false) цикл будет работать.
> Когда **.next()** вернет **{value: undefined, done: true}** цикл остановится
